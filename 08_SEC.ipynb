{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# pandas tutorial: What American companies make more money?\n",
    "\n",
    "Data analysis of the _US Securities and Exchange Commission_ fillings dataset\n",
    "\n",
    "<br/><center><img alt=\"\" src=\"img/sec.jpeg\"/></center><br/>\n",
    "\n",
    "The goal of this tutorial is to use the basics of pandas to extract from SEC submissions the net income (profit) of certain American companies, and analyze which companies are making more money, and where they are located.\n",
    "\n",
    "Available datasets:\n",
    "\n",
    "- **Submissions** (`data/sec/sub.txt.gz`): Each row is a submission to the SEC by a company\n",
    "  - `adsh`: Primary key, id of the filling\n",
    "  - `cik`: Identifier of the company\n",
    "  - `name`: Name of the company\n",
    "  - `countryba`: Country where the company is based\n",
    "  - `stprba`: State where the company is based\n",
    "  - `countryinc`: Country where the company is incorporated\n",
    "  - `stprinc`: State where the company is incorporated\n",
    "\n",
    "\n",
    "- **Tags** (`data/sec/tag.txt.gz`): Tags represent concepts in the financial statements\n",
    "  - `tag`: Identifier of the tag (or concept being reported (revenue, cost of goods sold,...)\n",
    "  - `version`: For standard tags, the taxonomy where they are defined\n",
    "  - `custom`: Whether it is a custom tag by the filler (or a standard tag from the taxonomy)\n",
    "  - `abstract`: Whether the tag represents non-numeric information\n",
    "  - `tlabel`: The label of the tag (provided by the taxonomy or by the filler)\n",
    "  - `doc`: Detailed definition of the tag\n",
    "\n",
    "- **Presentation of statements** (`data/sec/pre.txt.gz`): Each row represents a line in a submitted statement\n",
    "  - `adsh`: Primary key, id of the filling\n",
    "  - `stmt`: The kind of statement (income statement, balance sheet)\n",
    "  - `tag`: Identifier of the tag (or concept being reported (revenue, cost of goods sold,...)\n",
    "  - `version`: For standard tags, the taxonomy where they are defined\n",
    "\n",
    "- **Numbers** (`data/sec/num.txt.gz`): Each row is a data point\n",
    "  - `adsh`: Primary key, id of the filling\n",
    "  - `tag`: Identifier of the tag (or concept being reported (revenue, cost of goods sold,...)\n",
    "  - `version`: For standard tags, the taxonomy where they are defined\n",
    "  - `ddate`: The end date for the data value\n",
    "  - `uom`: The unit of measure the value is reported in\n",
    "  - `value`: The numerical value reported by the company\n",
    "\n",
    "[Complete dataset information](data/sec/readme.htm)\n",
    "\n",
    "Source: [SEC.gov | Financial Statement Data Sets](https://www.sec.gov/dera/data/financial-statement-data-sets.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <p><b>TASK 1</b>: Understand and clean the data about companies</p>\n",
    "    <img alt=\"\" src=\"img/companies.jpeg\"/>\n",
    "    <p>Tasks:\n",
    "        <ul>\n",
    "            <li>Load the data from <code>data/sec/sub.txt.gz</code>.</li> \n",
    "            <li>Check the size of the data.</li>\n",
    "            <li>Find the row(s) for <i>Bank of Hawaii</i>.</li>\n",
    "            <li>Create a column with the URL of the listing</li>\n",
    "            <li>Find the 10 companies that submitted more fillings (and where they are based).</li>\n",
    "            <li>Find which kind of forms exist, and leave only <i>10-K</i> and <i>10-Q</i> forms</li>\n",
    "            <li>Leave only the last record for each company.</li>\n",
    "            <li>Keep only the relevant columns</li>\n",
    "        </ul>\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the data from `data/sec/sub.txt.gz`.\n",
    "\n",
    "Things to consider:\n",
    "- The file is compressed, do we need to decompress it, or can pandas do it for us?\n",
    "- What if we have a url, can pandas download it for us?\n",
    "- In Linux/Mac you can preview the first lines of the file with `!zcat data/sec/sub.txt.gz | head -n 5`\n",
    "- In Windows, use `\\` for the path (or let Python build the path for you with `os.path.join`)\n",
    "- What kind of format has the file? How are columns separated? Does the first row contain the column names? \n",
    "- You need to import the `pandas` module\n",
    "- There [several functions](https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html) `pandas.read_*` to load data from different formats, which is the appropriate?\n",
    "- What parameters does the selected function expect?\n",
    "- Can we set the column `adsh` (the primary key) as the index?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check the size of the dataset (number of rows and columns)\n",
    "\n",
    "Things to consider:\n",
    "- Does the Python `len` work with `pandas` objects?\n",
    "- Can we use `len` for both rows and columns? How?\n",
    "- What do the next attributes report?\n",
    "  - `DataFrame.info()`\n",
    "  - `DataFrame.ndim`\n",
    "  - `DataFrame.count()`\n",
    "  - `DataFrame.shape`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a new column `url` that contains a link to the SEC website for the submission. The url is `http://www.sec.gov/Archives/edgar/data/{cik}/{accession}/` where:\n",
    "- `cik` is the identifier of the company (column `cik`)\n",
    "- `accession` is the identifier of the submission (column `adsh`) without the dashes (`-`), only the numerical values\n",
    "\n",
    "Things to consider:\n",
    "- To create a new column, just assign to the column, for example `my_dataframe['new_column'] = ...`\n",
    "- Operation with columns are possible, for example, to add `1` to every value in a column, you can perform `my_dataframe['my_column'] + 1`\n",
    "- To change the type of a column, the method `.astype()` exists (for example, a column with numbers as string can be converted to integers with `my_dataframe['numbers_as_str'].astype(int)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Find the data for _Bank of Hawaii_. We don't know the exact name of the company in the dataset, but we can assume:\n",
    "- The name of the company starts by `Bank` (we don't know about the case)\n",
    "- The name contains the word `Hawaii` (we don't know the case either)\n",
    "\n",
    "Things to consider:\n",
    "- To access a `DataFrame` column, we can provide the name in squared brackets, for example `my_dataframe['my_column']`\n",
    "- To filter a `DataFrame` we can provide a condition in squared brackets, for example `my_dataframe[my_dataframe['my_column'] == 'value']`\n",
    "- All Python string functions are available under the attribute `str`, for example `my_dataframe.str.strip()`.\n",
    "- Operators between objects are:\n",
    "  - And: `&`\n",
    "  - Or: `|`\n",
    "- Note that [Python operator precendence](https://docs.python.org/3/reference/expressions.html#operator-precedence) applies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Find the id, name and location of the 10 companies that submitted more fillings to the SEC. The exact columns we want to retrieve are:\n",
    "- cik\n",
    "- name\n",
    "- countryba\n",
    "- stprba\n",
    "- cityba\n",
    "- url\n",
    "\n",
    "Things to consider:\n",
    "- pandas has advanced functionality for aggregation, but to simply count number of occurrences `Series.value_counts()` can be used\n",
    "- The `n` first and last rows of a pandas object can be obtained by `.head(n)` and `.tail(n)`\n",
    "- As noted earlier, to access a column you can use `my_dataframe['my_colunm']`. But the index is not a column, and needs to be obtained as `my_dataframe.index`\n",
    "- The equivalent of `'my_value' in my_list` in pandas is `my_dataframe['my_column'].isin(my_list)`, which will return a boolean value for each value in the column\n",
    "- To deal with duplicated values, pandas provides methods such as `.duplicated()` which returns whether the row is duplicated, or `.drop_duplicates()` which deletes the ones that are"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check which forms companies submitted, and keep only `10-Q` forms.\n",
    "\n",
    "Things to consider:\n",
    "- The column with the type of form is `form`\n",
    "- Some of the types of forms are:\n",
    "  - `10-K`: Annual reports\n",
    "  - `10-Q`: Quarterly reports\n",
    "  - `8-K`: Current reports\n",
    "- As seen, selecting a column can be done with `my_dataframe['my_column']`\n",
    "- `.value_counts()` can give the unique values with the number of occurrences\n",
    "- `.drop_duplicates()` can be also used to find the list of unique values\n",
    "- As seen, we can compare columns like regular Python objects (e.g. `my_dataframe['my_column'] == value`)\n",
    "- We can use this conditions to filter the data with `my_dataframe[condition]`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a copy of the original dataset, leaving only for each company, the last submission.\n",
    "\n",
    "Things to consider:\n",
    "- We have seen before how to drop duplicates, but how to tell which rows to keep?\n",
    "- `.drop_duplicates()` has a parameter `keep` to control which rows to keep based on the order\n",
    "- To sort a dataframe by a column, use `.sort_values('my_column')` and to sort by the index use `.sort_index()`. Both accept a parameter `ascending` to control the order\n",
    "- To check if there are duplicates in the index, it is not needed to call `.duplicated().any()`. An attributed `.has_duplicates` exists\n",
    "- To check if the index is sorted, it can be used `.is_monotonic`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Keep only the relevant columns, and discard the rest. The columns we want to keep are:\n",
    "- cik\n",
    "- name\n",
    "- countryba\n",
    "- stprba\n",
    "- cityba\n",
    "\n",
    "Things to consider:\n",
    "- There are different ways to select columns in a dataframe\n",
    "- A pandas dataframe can usually be seen as a dictionary of columns, to select a column we can simply use `my_dataframe['my_column']`\n",
    "- A list can be provided instead of a string, and multiple columns can be selected at once\n",
    "- An attribute `.loc` exists, which can also select rows or columns, for example `.loc[['row1', 'row2'], ['col1', 'col2']]`\n",
    "- When indexing, `:` is an empty slice that usually means \"all\"\n",
    "- If the list of columns to drop is smaller than the ones to keep, it can be simpler to use `.drop()` and specify which columns to drop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### SOLUTIONS ###\n",
    "%load solutions/sec_1.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <p><b>TASK 2</b>: Load, filter and join the datasets with the financial information</p>\n",
    "    <img alt=\"\" src=\"img/finance.jpeg\"/>\n",
    "    <p>Tasks:\n",
    "        <ul>\n",
    "            <li>Load the data from <code>data/sec/pre.txt.gz</code></li>\n",
    "            <li>Leave only the rows of the income statements</li>\n",
    "            <li>Leave only statements using the US GAAP taxonomy</li>\n",
    "            <li>Join the data from the presentation of statements with the submissions</li>\n",
    "        </ul>\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the presentations of statements from the file `data/sec/pre.txt.gz` to a pandas object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Filter the dataset so only the rows corresponding to income statements are left.\n",
    "\n",
    "Things to consider:\n",
    "- The column specifying the type of statement is `stmt`, the possible values are:\n",
    "  - `BS`: Balance Sheet\n",
    "  - `IS`: Income Statement\n",
    "  - `CF`: Cash Flow\n",
    "  - `EQ`: Equity\n",
    "  - `CI`: Comprehensive Income\n",
    "  - `UN`: Unclassifiable Statement\n",
    "- Besides using `my_dataframe[condition]`, pandas dataframes can filter with the `.query()` method, which accepts the condition as a string with Python code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Find the taxonomy corresponding to US GAAP 2018, and filter all the rows corresponding to other taxonomies.\n",
    "\n",
    "Things to consider:\n",
    "- The column indicating the taxonomy is `version`\n",
    "- The value corresponding to US GAAP 2018 contains the text `GAAP` on it (in any possible case, `gaap`, `Gaap`,...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Join the data of the submissions, with the data of the presentation of statements.\n",
    "\n",
    "Things to consider:\n",
    "- pandas dataframes contain a method `.join()` that joins two dataframes.\n",
    "- By default, dataframes are joined by indices\n",
    "- A parameter `on` can be specified to join on other columns in the source dataframe"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### SOLUTIONS ###\n",
    "%load solutions/sec_2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <p><b>TASK 3</b>: Load, filter and join the numbers of the financial statements</p>\n",
    "    <img alt=\"\" src=\"img/financial_numbers.jpeg\"/>\n",
    "    <p>Tasks:\n",
    "        <ul>\n",
    "            <li>Load the numbers of the financial statements from <code>data/sec/num.txt.gz</code></li>\n",
    "            <li>Leave only revenue and net income of US GAAP statements</li>\n",
    "            <li>Keep only the tag and the value</li>\n",
    "            <li>Keep only the rows representing the company net income</li>\n",
    "            <li>Create a column with the income per quarter</li>\n",
    "            <li>Filter all the duplicate values except the last one</li>\n",
    "            <li>Join with the main dataframe</li>\n",
    "        </ul>\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the numbers of the financial statements from `data/sec/num.txt.gz`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Leave only the relevant rows:\n",
    "- Rows using the US GAAP taxonomy\n",
    "- Rows of the company net income\n",
    "- Values in US dollars\n",
    "\n",
    "Things to consider:\n",
    "- The tag for the net income is `NetIncomeLoss`\n",
    "- The units column is `uom`\n",
    "- The value for American dollars is `USD`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some values represent the value for more than one quarter. We need to normalize to a common unit \"Net income in USD in one quarter\" so we can compare different companies.\n",
    "\n",
    "Create a new column with the value (net income) per quarter.\n",
    "\n",
    "Things to consider:\n",
    "- The column containing the number of quarters is `qtrs`\n",
    "- You can operate with pandas objects in the same way as with Python objects (e.g. `my_dataframe['my_column'] + 1`)\n",
    "- To create a new column you can simply assign to it (e.g. `my_dataframe['new_column_name'] = ...`)\n",
    "- If it makes things simpler for you, you can drop the original columns `qtrs` and `value`, since we're not going to use them anymore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are values at different points in time. We are interested in the latest results of the company. Filter the rows that do not contain the latest one.\n",
    "\n",
    "Things to consider:\n",
    "- The point in time a value is refering to is specified in the column `ddate`\n",
    "- As we have seen before, `.drop_duplicates()` can remove duplicated values, and keep the first or the last (using the `keep` paramter)\n",
    "- To sort values and set the one we are interested in at the beginning or the end of its group, we can use `.sort_values()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Joing the main dataframe with this one.\n",
    "\n",
    "Things to consider:\n",
    "- In this case we want to join by both the adsh and the tag"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### SOLUTIONS ###\n",
    "%load solutions/sec_3.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <p><b>TASK 4</b>: Extract insights, analyze and visualize the data</p>\n",
    "    <img alt=\"\" src=\"img/financial_viz.jpeg\"/>\n",
    "    <p>Tasks:\n",
    "        <ul>\n",
    "            <li>Find the companies with the highest net income (profit)</li>\n",
    "            <li>Get the total and average net income for each state</li>\n",
    "            <li>Create a graphical visualization of the data by state</li>\n",
    "        </ul>\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get the list of the 5 companies that had the highest net income.\n",
    "\n",
    "Things to consider:\n",
    "- pandas has `.sort_values()` methods to sort by a column\n",
    "- Sorting a whole dataframe requires much more time and resources than just finding the highest or lowest values and for this reason pandas provides `.nlargest()` and `.nsmallest()` methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Group the data by state, and compute for each group (for each state) the average and the total net income.\n",
    "\n",
    "Things to consider:\n",
    "- The column with the state is `stprba`\n",
    "- pandas objects have a `.groupby()` method that receive as a parameter the column or columns to group by\n",
    "- A groupby object has methods to compute statistics, such as `.sum()`, `.mean()`, `.median()`,..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visualize the previous data in a graphical way.\n",
    "\n",
    "- pandas objects have a `.plot()` method, that receives as parameter the kind of plot\n",
    "- Common kinds of plots are `line`, `bar`, `barh`,...\n",
    "- The plots are generated using Matplotlib\n",
    "- In Jupyter, it may be needed to run the magic `%matplotlib inline` to let Matplotlib know that the output should be in Jupyter format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### SOLUTIONS ###\n",
    "%load solutions/sec_4.py"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
