import pandas

# Show the 5 companies with the highest net income
df.set_index('name').value.nlargest(5).to_frame().style.format('{:,}')

# Compute the total net income by state
total_income_by_state = df.groupby('stprba')['value'].sum().nlargest(20)

# Compute the average net income by state
average_income_by_state = df.groupby('stprba')['value'].mean().nlargest(20)

# Visualize in a bar chart the total income by state
total_income_by_state.plot(kind='barh')

# Visualize in a bar chart the average income by state
average_income_by_state.plot(kind='barh')
