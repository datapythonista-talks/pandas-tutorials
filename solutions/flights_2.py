import pandas

# Copy into the clipboard (e.g. Ctrl+C) the table of with the columns of the airports dataset
airport_columns = pandas.read_clipboard(header=None, sep='\t')[0].str.strip().tolist()
airports = pandas.read_csv(AIRPORTS_FNAME, names=airport_columns)
airports = airports.set_index('Airport ID')
airports.loc[airports.IATA.isin(['ATL', 'ORD', 'PEK']), 'Name']
