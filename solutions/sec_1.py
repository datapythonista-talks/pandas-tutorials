# Import the pandas module
import pandas

# Load the csv file into a pandas DataFrame (it's tab delimited), use `adsh` column as index
sub = pandas.read_csv('data/sec/sub.txt.gz', sep='\t', index_col='adsh')

# Show the number of rows and columns (not counting the index)
sub.shape

# Find the rows for Bank of Hawaii
sub[sub.name.str.upper().str.startswith('BANK')
    & sub.name.str.upper().str.contains('HAWAII')]

# Add URL to the SEC website for the filling
sub['url'] = ('http://www.sec.gov/Archives/edgar/data/'
              + sub['cik'].astype(str)
              + '/'
              + sub.index.str.replace('-', '')
              + '/')

# Information on the 10 companies that submitted more fillings to the SEC
(sub.loc[sub.cik.isin(sub['cik'].value_counts()
                                .head(10)
                                .index),
         ['cik', 'name', 'countryba', 'stprba', 'cityba']]
    .drop_duplicates(subset=['cik']))

# Show the kind of forms
sub['form'].value_counts()

# Consider only 10-Q form
sub = sub[sub.form.isin(['10-K', '10-Q'])]

# Check if the index contains duplicates
sub.index.has_duplicates

# Check if the index is sorted
sub.index.is_monotonic

# Leave the last row of every company
sub = sub.drop_duplicates(subset=['cik'], keep='last')

# Select the columns we want
sub = sub[['cik', 'name', 'countryba', 'stprba', 'cityba', 'url']]
