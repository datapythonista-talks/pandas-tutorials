import pandas

# Copy into the clipboard (e.g. Ctrl+C) the table of with the columns of the routes dataset
route_columns = pandas.read_clipboard(header=None)[0].str.strip().tolist()
routes = pandas.read_csv(ROUTES_FNAME, names=route_columns)
routes.head()
routes.info()
routes['Source airport'].value_counts().head(3)
routes['Destination airport'].value_counts().head(3)
