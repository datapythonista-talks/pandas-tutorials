routes = routes[routes['Source airport ID'].str.isdigit()].copy()
routes['Source airport ID'] = routes['Source airport ID'].astype(int)
routes.join(airports, on=['Source airport ID'])
