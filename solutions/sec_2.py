import pandas

# Load presentations of statements
pre = pandas.read_csv('data/sec/pre.txt.gz', sep='\t', index_col='adsh')

# Leave only income statements
pre = pre.query('stmt == "IS"')

# Find taxonomies containing "GAAP" (case-insensitive)
pre.loc[pre.version.str.lower().str.contains('gaap'), 'version'].drop_duplicates()

# Leave only US GAAP rows
pre = pre[pre.version.str.startswith('us-gaap/')]

# Filter rows that do not represent the revenue or the net profit
pre = pre[pre.tag == 'NetIncomeLoss']

# Keep only the statement, the taxonomy, and the tag with its label
pre = pre[['stmt', 'version', 'tag', 'plabel']]

# Join submissions with presentations of statements
df = sub.join(pre, how='inner')
