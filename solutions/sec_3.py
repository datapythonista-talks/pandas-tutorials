import pandas

# Load numbers of the statements
num = pandas.read_csv('data/sec/num.txt.gz', sep='\t', index_col='adsh')

# Filter and leave US GAAP for revenues and net income
num = num.loc[num.version.str.startswith('us-gaap/') & (num.tag == 'NetIncomeLoss'),
              ['tag', 'value', 'ddate', 'qtrs']]

# Compute value per quarter
num['value'] /= num['qtrs']

# Keep only the latest value of each submission
num = (num.reset_index()
          .sort_values(['adsh', 'ddate'])
          .drop_duplicates(subset=['adsh'], keep='last')
          .set_index(['adsh', 'tag'], drop=True)
          .drop(columns=['qtrs', 'ddate']))

# Join main dataframe with numbers of statements
df = df.set_index('tag', append=True).join(num, how='inner')
